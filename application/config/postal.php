<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
$config['message_types'] = array(
    'success' => array('<div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>'),
    'error' => array('<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>'),
    'message' => array('<div class="alert alert-primary alert-dismissible bg-primary text-white border-0 fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>'),
    'info' => array('<div class="alert alert-info alert-dismissible bg-info text-white border-0 fade show">', '</div>'));
