<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from coderthemes.com/minton/layouts/vertical/blue/dashboard-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Sep 2019 14:25:31 GMT -->
<head>
        <meta charset="utf-8" />
        <title>LMS </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.ico');?>">

        <!-- plugin css -->
        <link href="<?php echo base_url('assets/libs/jquery-vectormap/jquery-jvectormap-1.2.2.css');?>" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/icons.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/app.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/styles.css');?>" rel="stylesheet" type="text/css" />

    </head>

    <body>