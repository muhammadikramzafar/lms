   <!-- ========== Left Sidebar Start ========== -->
   <div class="left-side-menu">

<div class="slimscroll-menu">

    <!--- Sidemenu -->
    <div id="sidebar-menu">

        <ul class="metismenu" id="side-menu">

            <li class="menu-title">Navigation</li>
            <li>
                <a href="widgets.html" class="waves-effect">
                    <i class="remixicon-dashboard-line"></i>
                    <span> Dashboard </span>
                </a>
            </li>

            <!-- <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <i class="remixicon-dashboard-line"></i>
                    <span class="badge badge-success badge-pill float-right">2</span>
                    <span> Dashboards </span>
                </a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li>
                        <a href="index.html">Dashboard 1</a>
                    </li>
                    <li>
                        <a href="dashboard-2.html">Dashboard 2</a>
                    </li>
                </ul>
            </li> -->

            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <i class="remixicon-stack-line"></i>
                    <span> Leads </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li>
                        <a href="apps-kanbanboard.html">All</a>
                    </li>
                    <li>
                        <a href="apps-companies.html">Add</a>
                    </li>
                    <li>
                        <a href="apps-calendar.html">Calendar</a>
                    </li>
                    <li>
                        <a href="apps-filemanager.html">File Manager</a>
                    </li>
                    <li>
                        <a href="apps-tickets.html">Tickets</a>
                    </li>
                    <li>
                        <a href="apps-team.html">Team Members</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="widgets.html" class="waves-effect">
                    <i class="remixicon-vip-crown-2-line"></i>
                    <span> Widgets </span>
                </a>
            </li>

            <li class="menu-title mt-2">Components</li>

        </ul>

    </div>
    <!-- End Sidebar -->

    <div class="clearfix"></div>

</div>
<!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->