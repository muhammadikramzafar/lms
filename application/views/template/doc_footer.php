   <!-- Footer Start -->
   <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                               2016 - 2019 &copy; Minton theme by <a href="#">Coderthemes</a> 
                            </div>
                            <div class="col-md-6">
                                <div class="text-md-right footer-links d-none d-sm-block">
                                    <a href="javascript:void(0);">About Us</a>
                                    <a href="javascript:void(0);">Help</a>
                                    <a href="javascript:void(0);">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->


        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="<?php echo base_url('assets/js/vendor.min.js');?>"></script>

        <script src="<?php echo base_url('assets/libs/apexcharts/apexcharts.min.js');?>"></script>
        <script src="<?php echo base_url('assets/libs/jquery-sparkline/jquery.sparkline.min.js');?>"></script>
        <script src="<?php echo base_url('assets/libs/jquery-vectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
        <script src="<?php echo base_url('assets/libs/jquery-vectormap/jquery-jvectormap-world-mill-en.js');?>"></script>

        <!-- Peity chart-->
        <script src="<?php echo base_url('assets/libs/peity/jquery.peity.min.js');?>"></script>

        <!-- init js -->
        <script src="<?php echo base_url('assets/js/pages/dashboard-2.init.js');?>"></script>

        <!-- App js -->
        <script src="<?php echo base_url('assets/js/app.min.js');?>"></script>
        
    </body>

</html>