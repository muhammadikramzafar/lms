
            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <!-- <div class="page-title-left">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Minton</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                            <li class="breadcrumb-item active">Dashboard 2</li>
                                        </ol>
                                    </div> -->
                                    <h4 class="page-title"><?php  echo isset($page_title)? $page_title : ''; ?></h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 




<!-- ------------------------------------- PAge START ---------------------------------------- -->
<div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                    
                                   

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="p-2">
                                         
                                            <?php echo form_open("auth/create_group");?>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="group_name">Group Name</label>
                                                        <div class="col-sm-10">
                                                            <?php  echo form_input('group_name',set_value('group_name'),'class="form-control" id="group_name"'); ?>
                                                            <?php echo form_error('group_name', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="description">Description</label>
                                                        <div class="col-sm-10">
                                                            <?php  echo form_input('description',set_value('description'),'class="form-control" id="description"'); ?>
                                                            <?php echo form_error('description', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>
                                                  
                                                            
                                                  
                                                    <div class="form-group row">
                                                        <?php echo form_submit('submit', lang('create_group_submit_btn'), 'class="btn btn-success"');?>
                                                    </div>

                                                 <?php echo form_close();?>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- end row -->

                                </div> <!-- end card-box -->
                            </div><!-- end col -->
                        </div>
