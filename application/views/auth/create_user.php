

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <!-- <div class="page-title-left">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Minton</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                            <li class="breadcrumb-item active">Dashboard 2</li>
                                        </ol>
                                    </div> -->
                                    <h4 class="page-title"><?php  echo isset($page_title)? $page_title : ''; ?>
                                    <a href="<?php echo site_url('auth/create_user'); ?>" class="btn btn-success" >New</a>
                                </h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 
<!-- ------------------------------------- PAge START ---------------------------------------- -->
<div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                    
                                <?php  echo $this->postal->get();  ?>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="p-2">
                                            <?php echo form_open("auth/create_user");?>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="simpleinput">First Name</label>
                                                        <div class="col-sm-10">
                                                            <?php  echo form_input('first_name',set_value('first_name'),'class="form-control"'); ?>
                                                            <?php echo form_error('first_name', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="simpleinput">Last Name</label>
                                                        <div class="col-sm-10">
                                                            <?php  echo form_input('last_name',set_value('last_name'),'class="form-control"'); ?>
                                                            <?php echo form_error('last_name', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>
                                                  
                                                            
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="example-email">User Name</label>
                                                        <div class="col-sm-10">
                                                            <?php  echo form_input('identity',set_value('identity'),'class="form-control"'); ?>
                                                            <?php echo form_error('identity', '<div class="text-danger">', '</div>'); ?>
                                                          
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="example-email">Compnay</label>
                                                        <div class="col-sm-10">
                                                            <?php  echo form_input('company',set_value('company'),'class="form-control"'); ?>
                                                            <?php echo form_error('company', '<div class="text-danger">', '</div>'); ?>
                                                          
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="example-email">Phone</label>
                                                        <div class="col-sm-10">
                                                            <?php  echo form_input('phone',set_value('phone'),'class="form-control"'); ?>
                                                            <?php echo form_error('phone', '<div class="text-danger">', '</div>'); ?>
                                                          
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="example-email">Email</label>
                                                        <div class="col-sm-10">
                                                            
                                                            <?php  echo form_input('email',set_value('email'),'class="form-control"'); ?>
                                                            <?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="example-password">Password</label>
                                                        <div class="col-sm-10">
                                                            <?php echo form_password('password','','class="form-control"'); ?>
                                                            <?php echo form_error('password', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="example-password">Password</label>
                                                        <div class="col-sm-10">
                                                            <?php echo form_password('password_confirm','','class="form-control"'); ?>
                                                            <?php echo form_error('password_confirm', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <?php echo form_submit('submit', lang('create_user_submit_btn'), 'class="btn btn-success"');?>
                                                    </div>

                                                 

                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- end row -->

                                </div> <!-- end card-box -->
                            </div><!-- end col -->
                        </div>
