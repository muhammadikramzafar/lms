
            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <!-- <div class="page-title-left">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Minton</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                            <li class="breadcrumb-item active">Dashboard 2</li>
                                        </ol>
                                    </div> -->
                                    <h4 class="page-title"><?php  echo isset($page_title)? $page_title : ''; ?></h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 
<!-- ------------------------------------- PAge START ---------------------------------------- -->
<div class="row">
    <div class="col-12">
        <a href="<?php echo site_url('auth/create_user');?>" class="btn btn-success m-10" >New User</a>
        <?php  echo $this->postal->get();  ?>
        <div class="card-box">
       
            <div class="table-responsive">

                <table class="table table-sm mb-0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Company</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Roles</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($all_users)) { ?>
                            <?php foreach($all_users as $key => $user){ ?>
                                <tr>
                                    <td><?php echo $user->id; ?></td>
                                    <td><?php echo $user->first_name.' '.$user->last_name; ?></td>
                                    <td><?php echo $user->username;?></td>
                                    <td><?php echo $user->company; ?></td>
                                    <td><?php echo $user->email; ?></td>
                                    <td><?php echo $user->phone; ?></td>
                                    <th>
                                        <?php 
                                            $user_groups = $this->ion_auth->get_users_groups($user->id)->result();
                                            foreach($user_groups as $grp){
                                                echo "<span class='badge badge-success'>".$grp->name."</span>";
                                            }

                                        ?>
                                    </th>
                                    <td><?php echo ($user->active ==1)? 'Active' : ''; ?></td>
                                    <td>
                                    <a href="<?php echo site_url('auth/edit_user/'.$user->id); ?>" class="btn btn-xs btn-secondary"><i class="mdi mdi-pencil"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>    
                         
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>