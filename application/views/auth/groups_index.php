
            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <!-- <div class="page-title-left">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Minton</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                            <li class="breadcrumb-item active">Dashboard 2</li>
                                        </ol>
                                    </div> -->
                                    <h4 class="page-title"><?php  echo isset($page_title)? $page_title : ''; ?></h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 
<!-- ------------------------------------- PAge START ---------------------------------------- -->
<div class="row">
    <div class="col-12">
        <a href="<?php echo site_url('auth/create_group');?>" class="btn btn-success m-10" >New Group</a>
        <div class="card-box">
        <?php  echo $this->postal->get();  ?>
          
            <div class="table-responsive">

                <table class="table table-sm mb-0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Role Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($all_groups)) { ?>
                            <?php foreach($all_groups as $key => $group){ ?>
                                <tr>
                                    <td><?php echo $group->id; ?></td>
                                    <td><?php echo $group->name ?></td>
                                    <td><?php echo $group->description;?></td>
                                   
               
                                    <td>
                                        <a href="<?php echo site_url('auth/edit_group/'.$group->id); ?>" class="btn btn-xs btn-secondary"><i class="mdi mdi-pencil"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>    
                         
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

                        <!-- end row -->
                        <!-- ------------------------------ Content Ended --------------------------- -->
                   
             