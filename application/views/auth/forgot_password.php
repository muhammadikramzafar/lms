<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta charset="utf-8" />
        <title>LMS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Muhammad Ikram Zafar" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.ico');?>" />
        <!-- App css -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/icons.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/app.min.css'); ?>" rel="stylesheet" type="text/css" />

    </head>


    <body>

        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card">

                            <div class="card-body p-4">
                            
                                <div class="text-center w-75 m-auto">
                                    <a href="index.html">
                                        <span><img src="<?php echo base_url('assets/images/logo-dark.png'); ?>" alt="" height="22"></span>
                                    </a>
                                    <p class="text-muted mb-4 mt-3">Enter your email address and we'll send you an email with instructions to reset your password.</p>
                                </div>

                                <?php echo form_open("auth/forgot_password");?>

                                    <div class="form-group mb-3">
                                        <label for="emailaddress">Email address</label>
                                        <?php echo form_input($identity,'','class="form-control" id="emailaddress" required="" placeholder="Enter your email"');?>
                                    </div>

                                    <div class="form-group mb-0 text-center">
  
                                        <?php echo form_submit('submit', 'Reset Password',' class="btn btn-primary btn-block"');?> 
                                        <!-- lang('forgot_password_submit_btn') -->
                                    </div>

                                    <?php echo form_close();?>

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        <div class="row mt-3">
                            <div class="col-12 text-center">
                                <p class="text-muted">Back to <a href="<?php echo site_url('auth/login');?>" class="text-muted font-weight-medium ml-1">Log in</a></p>
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->


        <footer class="footer footer-alt">
            2016 - <?php echo date('Y');?> &copy; LMS by <a href="https://muhammadikramzafar.com" target="_blank" class="text-muted">MuhammadIkramZafar</a> 
        </footer>

        <!-- Vendor js -->
        <script src="<?php echo base_url('assets/js/vendor.min.js');?>"></script>

        <!-- App js -->
        <script src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
        
    </body>

<!-- Mirrored from coderthemes.com/minton/layouts/vertical/blue/pages-recoverpw.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Sep 2019 14:25:45 GMT -->
</html>
