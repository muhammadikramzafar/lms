
            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <!-- <div class="page-title-left">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Minton</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                            <li class="breadcrumb-item active">Dashboard 2</li>
                                        </ol>
                                    </div> -->
                                    <h4 class="page-title"><?php  echo isset($page_title)? $page_title : ''; ?></h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 




<!-- ------------------------------------- PAge START ---------------------------------------- -->
<div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                    
                                <?php  echo $this->postal->get();  ?>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="p-2">
                                            <?php echo form_open(current_url());?>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="group_name">Group Name</label>
                                                        <div class="col-sm-10">
                                                            <?php  echo form_input('group_name',set_value('group_name',$group_name['value']),'class="form-control" id="group_name"'); ?>
                                                            <?php echo form_error('group_name', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="group_description">Description</label>
                                                        <div class="col-sm-10">
                                                            <?php  echo form_input('group_description',set_value('group_description',$group_description['value']),'class="form-control" id="group_description"'); ?>
                                                            <?php echo form_error('group_description', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>
                                                  
                                                            
                                                  
                                                    <div class="form-group row">
                                                        <?php echo form_submit('submit', lang('edit_group_submit_btn'), 'class="btn btn-success"');?>
                                                    </div>

                                                 <?php echo form_close();?>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- end row -->

                                </div> <!-- end card-box -->
                            </div><!-- end col -->
                        </div>









