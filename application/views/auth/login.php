<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from coderthemes.com/minton/layouts/vertical/blue/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Sep 2019 14:25:44 GMT -->
<head>
        <meta charset="utf-8" />
        <title>LMS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Muhammad Ikram Zafar" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.ico');?>" />
        <!-- App css -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/icons.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/app.min.css'); ?>" rel="stylesheet" type="text/css" />

    </head>

    <body>

        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card">

                            <div class="card-body p-4">
                           
                                <div class="text-center w-75 m-auto">
                                    <a href="#">
                                        <span><img src="<?php echo base_url('assets/images/logo-dark.png');?>" alt="" height="22"></span>
                                    </a>
                                    <p class="text-muted mb-4 mt-3">Enter your email address and password to access admin panel.</p>
                                    <?php  echo $this->postal->get();  ?>
                                </div>

                                <?php echo form_open("auth/login");?>

                                    <div class="form-group mb-3">
                                        <label for="emailaddress">User Name</label>
                                       
                                        <?php echo form_input($identity,'username','class="form-control" placeholder="username"');?>
                                    </div>

                                    <div class="form-group mb-3">
                                        <label for="password">Password</label>
                                        <?php echo form_input($password,'','class="form-control" placeholder="Enter your password"');?>
                                    </div>

                                    <div class="form-group mb-3">
                                        <div class="custom-control custom-checkbox">
                                            <?php echo form_checkbox('remember', '1', FALSE, 'class="custom-control-input" id="checkbox-signin"');?>
                                            <label class="custom-control-label" for="checkbox-signin">Remember me</label>
                                        </div>
                                    </div>

                                    <div class="form-group mb-0 text-center">
                                       
                                        <?php echo form_submit('submit', lang('login_submit_btn'),'class="btn btn-primary btn-block"');?>
  
                                    </div>

                                    <?php echo form_close();?>

                                <!-- <div class="text-center">
                                    <h5 class="mt-3 text-muted">Sign in with</h5>
                                    <ul class="social-list list-inline mt-3 mb-0">
                                        <li class="list-inline-item">
                                            <a href="javascript: void(0);" class="social-list-item border-primary text-primary"><i class="mdi mdi-facebook"></i></a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="javascript: void(0);" class="social-list-item border-danger text-danger"><i class="mdi mdi-google"></i></a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="javascript: void(0);" class="social-list-item border-info text-info"><i class="mdi mdi-twitter"></i></a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="javascript: void(0);" class="social-list-item border-secondary text-secondary"><i class="mdi mdi-github-circle"></i></a>
                                        </li>
                                    </ul>
                                </div> -->

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        <div class="row mt-3">
                            <div class="col-12 text-center">
                                <p> <a href="<?php echo site_url('auth/forgot_password'); ?>" class="text-muted ml-1"><?php echo lang('login_forgot_password');?></a></p>
                                <!-- <p class="text-muted">Don't have an account? <a href="pages-register.html" class="text-primary font-weight-medium ml-1">Sign Up</a></p> -->
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->


        <footer class="footer footer-alt">
            2016 - <?php echo date('Y');?> &copy; LMS by <a href="https://muhammadikramzafar.com" target="_blank" class="text-muted">MuhammadIkramZafar</a> 
        </footer>

        <!-- Vendor js -->
        <script src="<?php echo base_url('assets/js/vendor.min.js');?>"></script>

        <!-- App js -->
        <script src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
        
    </body>

<!-- Mirrored from coderthemes.com/minton/layouts/vertical/blue/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Sep 2019 14:25:45 GMT -->
</html>