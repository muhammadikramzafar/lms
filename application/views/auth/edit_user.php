
            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <!-- <div class="page-title-left">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Minton</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                            <li class="breadcrumb-item active">Dashboard 2</li>
                                        </ol>
                                    </div> -->
                                    <h4 class="page-title"><?php  echo isset($page_title)? $page_title : ''; ?></h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 
<!-- ------------------------------------- PAge START ---------------------------------------- -->


                              <div class="row">

                                        <div class="col-12">
                                            <div class="p-2">
                                            <?php echo form_open(uri_string());?>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="first_name">First Name</label>
                                                        <div class="col-sm-10">
          
                                                            <?php echo form_input('first_name',set_value('first_name',$first_name['value']),'class="form-control" id="first_name"'); ?>
                                                            <?php echo form_error('first_name', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                      <label class="col-sm-2 col-form-label" for="last_name">Last Name</label>
                                                        <div class="col-sm-10">
                                                            <?php echo form_input('last_name',set_value('last_name',$last_name['value']),'class="form-control" id="last_name"'); ?>
                                                            <?php echo form_error('last_name', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                      <label class="col-sm-2 col-form-label" for="company_name">Company Name</label>
                                                        <div class="col-sm-10">

                                                            <?php echo form_input('company',set_value('company',$company['value']),'class="form-control" id="company_name"');?>
                                                            <?php echo form_error('company', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                      <label class="col-sm-2 col-form-label" for="phone">Phone</label>
                                                        <div class="col-sm-10">
          
                                                            <?php echo form_input('phone',set_value('phone',$phone['value']),'class="form-control" id="phone"');?>
                                                            <?php echo form_error('phone', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>

                                                   

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="password">Password</label>
                                                        <div class="col-sm-10">
                                                            <?php echo form_password('password','','class="form-control" id="password"'); ?>
                                                            <?php echo form_error('password', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label" for="example-password">Confirm Password</label>
                                                        <div class="col-sm-10">
                                                            <?php echo form_password('password_confirm','','class="form-control" id'); ?>
                                                            <?php echo form_error('password_confirm', '<div class="text-danger">', '</div>'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                    
                                                        <?php if ($this->ion_auth->is_admin()): ?>
                                                        <label class="col-sm-2 col-form-label" for="example-password"><?php echo lang('edit_user_groups_heading');?></label>
                                                        <div class="col-sm-10">
                                                        <?php foreach ($groups as $group):?>
                                                            
                                                            <?php
                                                                $gID=$group['id'];
                                                                $checked = null;
                                                                $item = null;
                                                                foreach($currentGroups as $grp) {
                                                                    if ($gID == $grp->id) {
                                                                        $checked= ' checked="checked"';
                                                                    break;
                                                                    }
                                                                }
                                                            ?>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox"  class="custom-control-input" id="customCheck-<?php echo $gID?>"  name="groups[]" value="<?php echo $group['id'];?>" <?php echo $checked;?> />
                                                                <label class="custom-control-label" for="customCheck-<?php echo $gID?>"> <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?></label>
                                                            </div>
                                        
                                                         
                                                        <?php endforeach?>
                                                        </div>

                                                        <?php endif ?>
                                                    </div>

                                                    <?php echo form_hidden('id', $user->id);?>
                                                    <?php echo form_hidden($csrf); ?>

                                                        <?php echo form_submit('submit', lang('edit_user_submit_btn'),'class="btn btn-success');?>
                                                   

                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- end row -->
