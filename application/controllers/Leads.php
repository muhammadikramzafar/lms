<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leads extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!$this->ion_auth->logged_in()){  
			redirect('/auth/login');
		}
		$this->data['user'] = $this->ion_auth->user()->row();
		// $this->data['parent_user']=$this->ion_auth->user($this->ion_auth->user()->row()->parent_user)->row(); 
       
	}
	public function index()
	{
		$this->data['page'] = 'leads/all_leads_view';
		$this->data['page_title'] = 'All Leads';
		$this->data['all_leads'] 
		$this->load->view('template',$this->data);
	}
}
