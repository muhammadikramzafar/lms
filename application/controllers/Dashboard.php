<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!$this->ion_auth->logged_in()){  
			redirect('/auth/login');
		}
		$this->data['user'] = $this->ion_auth->user()->row();
		// $this->data['parent_user']=$this->ion_auth->user($this->ion_auth->user()->row()->parent_user)->row(); 
       
	}
	public function index()
	{
		
		// $data['result'] = $this->product_model->month_wise_product();
		// $data['user_data'] = $this->user_model->get_user($sess_id['user_id']);
		//echo "<pre>";print_r($data['user']);die;
		$this->data['page'] = 'dashboard_view';
		$this->data['page_title'] = 'Dashboard';
		// echo "<pre>";print_r($this->data);die;
		$this->load->view('template',$this->data);
	}
}
